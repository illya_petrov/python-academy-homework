"""2) Реализуйте модуль word_utils.py, позволяющий:
- очистить предложение от знаков препинания;
- получить список слов из предложения;
- получить самое длинное слово в предложении;"""


def no_comma(in_str: str, splitter: str = '') -> str:
    for i in in_str:
        if i in """,.!:;[]{}\/""":
            in_str = in_str.replace(i, splitter)
    return in_str


def word_list(in_str: str) -> list:
    out_str = no_comma(in_str, splitter=' ')
    out_list = out_str.split(' ')
    out_list = [i for i in out_list if i != '']
    return out_list


def longest_word(in_str: str) -> str:
    out_str = no_comma(in_str, splitter=' ')
    out_list = word_list(out_str)
    out_str = out_list[0]
    for i in out_list:
        if len(i) > len(out_str):
            out_str = i
    return out_str