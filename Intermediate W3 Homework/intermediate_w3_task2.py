"""2) Реализуйте модуль word_utils.py, позволяющий:
- очистить предложение от знаков препинания;
- получить список слов из предложения;
- получить самое длинное слово в предложении;"""

import word_utils
in_str = input('Please input any string: ')
print(f'This string without punctuation - {word_utils.no_comma(in_str)}\n'
      f'This string split in list or words - {word_utils.word_list(in_str)}\n'
      f'The longest word from this string - {word_utils.longest_word(in_str)}')
