"""1) Напишите программу, которая будет считывать содержимое файла, добавлять к считанным строкам порядковый
номер и сохранять их в таком виде в новом файле. Имя исходного файла необходимо запросить у пользователя,
так же как и имя целевого файла. Каждая строка в созданном файле должна начинаться с ее номера,
двоеточия и пробела, после чего должен идти текст строки из исходного файла."""


def check_input(file_path: str) -> bool:
    try:
        with open(file_path, 'r') as file:
            pass
        return True
    except TypeError:
        pass
    except FileNotFoundError:
        print("input: File not found")
        return False
    except IsADirectoryError:
        print("input: Invalid address")
        return False


def get_str(file_path: str):
    with open(file_path, 'r') as f:
        f_line = f.readline()
        while f_line != '':
            yield f_line
            f_line = f.readline()


def generate_file(file_path, n_str):
    try:
        file_transfer(file_path, n_str)
    except FileNotFoundError:
        print("output: File with this name can't be created")
    except IsADirectoryError:
        print("output: Invalid address")
    except FileExistsError:
        print("output: This file already exist")


def file_transfer(file_path: str, n_str):
    with open(file_path, 'x') as out_file:
        i = 1
        while True:
            try:
                out_file.write(f'{i}: {next(n_str)}')
                i += 1
            except StopIteration:
                print('Done')
                break


input_path = 'input_file.txt'
output_path = 'output_file.txt'

if check_input(input_path):
    input_str = get_str(input_path)
    generate_file(output_path, input_str)

