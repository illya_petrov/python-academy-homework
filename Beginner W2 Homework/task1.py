def dict_reverse(dict_origin: dict):
    dict_target = dict()
    for i in dict_origin:
        dict_target[dict_origin[i]] = i
    return dict_target


school = {'1а': 30,
          '2б': 24,
          '3б': 36,
          '1б': 29,
          '6а': 20,
          '7в': 34}
# в одном из классов изменилось количество учащихся
school['1а'] = 32

# в школе появился новый класс(добавьте новый клас)
school['5а'] = 27

# в школе был расформирован (удален) другой класс.
school.pop('3б')

# Вычислите и выведите общее количество учащихся в
# школе.
total_in_school = sum(x for x in school.values())

# Напишите функцию, которая принимает один словарь, и возвращает другой,
# в котором ключами являются значения из первого словаря, а значениями –
# соответствующие им ключи. Создайте словарь, передайте его в
# функцию.

print(f"Original dict - {school}")
print(f"Reversed dict - {dict_reverse(school)}")
