# Напишите программу, которая циклично запрашивает у пользователя номера
# символов по таблице Unicode и выводит соответствующие им символы.
# Завершает работу при вводе нуля.

def uni_symbol():
    s_value = int(input('Enter unicode number - '))
    if s_value == 0:
        return print('Program stopped')
    print(chr(s_value))
    return uni_symbol()


uni_symbol()