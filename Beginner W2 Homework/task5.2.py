# Напишите программу, которая измеряет длину введенной строки. Если
# строка длиннее десяти символов, то выносится предупреждение. Если
# короче, то к строке добавляется столько символов *, чтобы ее длина
# составляла десять символов, после чего новая строка должна выводиться
# на экран.

def check_len(check_str):
    if len(check_str) > 10:
        return print('Warning! This string is longer than 10 symbols')
    check_str += '*'*(10 - len(check_str))
    return print(check_str)


check_len(input('Please enter string - '))
