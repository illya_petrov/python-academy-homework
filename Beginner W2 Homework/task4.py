# Функция getInput не имеет параметров, запрашивает ввод с клавиатуры и
# возвращает в основную программу полученную строку.

def getInput():
    return input()

# Функция testInput имеет один параметр. В теле она проверяет, можно ли
# переданное ей значение преобразовать к целому числу. Если можно,
# возвращает логическое True. Если нельзя – False.

def testInput(value):
    if value.count('.') > 1:
        return False
    if "-" in value[1:]:
        return False
    value = value.replace('.', '')
    value = value.replace('-', '')
    if value.isnumeric():
        return value.isnumeric()

# Функция strToInt имеет один параметр. В теле преобразовывает переданное
# значение к целочисленному типу. Возвращает полученное число.

def strToInt(str_int):
    str_int = float(str_int)
    return int(round(str_int))

# Функция printInt имеет один параметр. Она выводит переданное значение
# на экран и ничего не возвращает.

def printInt(value_print):
    return print(value_print)

# В основной ветке программы вызовите первую функцию. То, что она
# вернула, передайте во вторую функцию. Если вторая функция вернула True,
# то те же данные (из первой функции) передайте в третью функцию, а
# возвращенное третьей функцией значение – в четвертую.


input_value = getInput()
if testInput(input_value):
    printInt(strToInt(input_value))
else:
    print('Not a suitable value')
