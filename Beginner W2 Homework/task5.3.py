# Напишите программу, которая запрашивает у пользователя шесть
# вещественных чисел. На экран выводит минимальное и максимальное из них,
# округленные до двух знаков после запятой. Выполните задание без
# использования встроенных функций min() и max().

def max_value(num_list):
    a = num_list[0]
    for i in num_list:
        if i > a:
            a = i
    return a


def min_value(num_list):
    a = num_list[0]
    for j in num_list:
        if j < a:
            a = j
    return a


n_list = []
for n in range(6):
    n_list.append(float(input(f'Enter float number {n+1} - ')))

print(f'Max value is - {round(max_value(n_list),2)}')
print(f'Min value is - {round(min_value(n_list),2)}')
