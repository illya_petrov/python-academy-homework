"""
1. Создать класс Point2D. Координаты точки задаются 2 параметрами -
координатами x, y на плоскости.
2. Реализовать метод distance который принимает экземпляр класса Point2D и
рассчитывает расстояние между 2мя точками на плоскости.
3. Создать защищенный атрибут класса - счетчик созданных экземпляров класса.
4. Чтение количества экземпляров реализовать через метод getter.
5. Создать класс Point3D, который является наследником класса Point2D.
Координаты точки задаются 3 параметрами - координатами x, y, z в пространстве.
6. Переопределить конструктор с помощью super().
7. Переопределить метод distance для определения расстояния между 2-мя точками в пространстве.
"""


class Point2D:
    __point2d_counter = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y
        Point2D.__point2d_counter += 1

    def distance(self, other_point):
        return ((self.x - other_point.x) ** 2 + (self.y - other_point.y) ** 2) ** 0.5

    @staticmethod
    def get_counter():
        return Point2D.__point2d_counter


class Point3D(Point2D):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def distance(self, other_point):
        return (((self.x - other_point.x) ** 2 + (self.y - other_point.y) ** 2 +
                (self.z - other_point.z) ** 2) ** 0.5)


point2d1 = Point2D(0, 0)
point2d2 = Point2D(3, 4)
print(f'2D point 1 - {point2d1.x, point2d1.y},\n'
      f'2D point 2 - {point2d2.x, point2d2.y},\n'
      f'Points created - {Point2D.get_counter()},\n'
      f'Distance - {point2d1.distance(point2d2)}\n')

point3d1 = Point3D(0, 0, 0)
point3d2 = Point3D(3, 2, 1)
print(f'3D point 1 - {point3d1.x, point3d1.y, point3d1.z},\n'
      f'3D point 2 - {point3d2.x, point3d2.y, point3d2.z},\n'
      f'Points created - {Point2D.get_counter()},\n'
      f'Distance - {point3d1.distance(point3d2)}')







