"""
1. Создать класс дробь(Fraction), конструктор которого принимает целые числа (num, den  -  числитель(numerator),
знаменатель(denominator) ).
Выполнить
2. Атрибуты числитель и знаменатель в классе сделать приватными.
3. Доступ к атрибутам реализовать через свойства.
4. Переопределить методы __sub__, __add__, __mull__, __truediv__
для того, чтобы можно было выполнять соответствующие математические действия с объектами класса дробь.
(Вычитание, сложение, умножение и деление).
5. В миксине реализовать статические методы, для этих же операций(add, sub, mull, div).
6. Добавить в класс миксин.
7. Создать classmethod который из строки типа 'числитель/знаменатель' возвращает объект класса дробь.
8. Переопределить метод __str__, который при выводе объекта на печать будет выводить строку вида num / den.
9. Создать объекты класса дробь.
10. Выполнить все реализованные методы.
"""


class Fraction:
    def __init__(self, num, den):
        self.set_num(num)  # числитель
        self.set_den(den)  # знаменатель

    def get_num(self):
        return self.__num

    def set_num(self, num):
        self.__num = num

    def get_den(self):
        return self.__den

    def set_den(self, den):
        if den < 0:
            self.__num *= -1
            den *= -1
        assert den != 0, '''Denominator can't be 0'''
        self.__den = den
        pass

    @classmethod
    def from_str(cls, fr_str):
        if cls.check_str(fr_str):
            num, den = fr_str.split('/')
            return cls(int(num), int(den))

    @staticmethod
    def check_str(fr_str):
        assert fr_str.count('/') == 1 and len(fr_str) >= 3, 'Not a fraction. Please use x/y format'
        num, den = fr_str.split('/')
        assert Fraction.check_value_str(num), 'Numerator should be integer'
        assert Fraction.check_value_str(den), 'Denominator should be integer'
        return True

    @staticmethod
    def check_value_str(value_str):
        if (value_str[0] == '-' and value_str[1:].isnumeric()) or value_str.isnumeric():
            return True

    def __str__(self):
        self.__num, self.__den = self.shorten(self.__num, self.__den)
        if self.__num == 0:
            return '0'
        elif self.__den == 1:
            return str(self.__num)
        else:
            return f'{self.__num}/{self.__den}'

    @staticmethod
    def shorten(num, den):
        i = 2
        while i <= min(abs(num), abs(den)):
            if num/i == num//i and den/i == den//i:
                num = num//i
                den = den//i
                i = 2
            else:
                i += 1
        return num, den

    def __sub__(self, other):
        temp_num = self.__num * other.__den - other.__num * self.__den
        temp_den = self.__den * other.__den
        return Fraction(temp_num, temp_den)

    def __add__(self, other):
        temp_num = self.__num * other.__den + other.__num * self.__den
        temp_den = self.__den * other.__den
        return Fraction(temp_num, temp_den)

    def __mul__(self, other):
        temp_num = self.__num * other.__num
        temp_den = self.__den * other.__den
        return Fraction(temp_num, temp_den)

    def __truediv__(self, other):
        temp_num = self.__num * other.__den
        temp_den = self.__den * other.__num
        if temp_den == 0:
            return '''Can't divide by 0'''
        return Fraction(temp_num, temp_den)


class Action:
    @staticmethod
    def add(v1, v2):
        return v1 + v2

    @staticmethod
    def sub(v1, v2):
        return v1 - v2

    @staticmethod
    def mul(v1, v2):
        return v1 * v2

    @staticmethod
    def div(v1, v2):
        return v1 / v2


class FractionExtended(Fraction, Action):
    pass

fract1 = FractionExtended(1, 2)
print(f'Fraction 1 = {fract1}')
fract2 = FractionExtended.from_str(input('Please enter Fraction 2 in x/y format: '))
print(f'Fraction 2 = {fract2}')
print(f'{fract1} + {fract2} = {fract1 + fract2}')
print(f'{fract1} - {fract2} = {fract1 - fract2}')
print(f'{fract1} * {fract2} = {fract1 * fract2}')
print(f'{fract1} / {fract2} = {fract1 / fract2}')
print(f'{fract1} add {fract2} = {FractionExtended.add(fract1, fract2)}')
print(f'{fract1} sub {fract2} = {FractionExtended.sub(fract1, fract2)}')
print(f'{fract1} mul {fract2} = {FractionExtended.mul(fract1, fract2)}')
print(f'{fract1} div {fract2} = {FractionExtended.div(fract1, fract2)}')

