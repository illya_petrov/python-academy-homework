"""1) Необходимо написать функцию калькулятор, которая принимает строку состоящую из числа, оператора
и второго числа разделенных пробелом. Например ('1 + 1'); Необходимо разделить строку используя str.split(),
и проверить является результирующий список валидным.
a) Если ввод не состоит из 3 элементов, необходимо возбудить исключение FormulaError,
которое является пользовательским исключением.
b) Попытайтесь сконвертировать первое и третье значение ввода к типу float.
Перехватите любые исключения типа ValueError, которые возникают, и выбросите FormulaError
c) Если второе значение ввода не является '+', '-', '*', '/' также выбросите FormulaError.
Если инпут валидный - ф-я должна вернуть результат операции"""


class FormulaError(Exception):
    def __init__(self, message='Formula Error'):
        self.message = message
        super().__init__(self.message)


def parse_eq(input_str: str) -> list:
    try:
        a, sign, b = input_str.split(' ')
        out_list = [a, sign, b]
        return out_list
    except ValueError:
        raise FormulaError


def convert_to_float(input_str: str) -> float:
    try:
        value = float(input_str)
        return value
    except ValueError:
        raise FormulaError


def sign_to_act(a: float, input_str: str, b: float) -> float:
    sign_dict = {
        '-': a - b,
        '+': a + b,
        '*': a * b,
        '/': a / b
    }
    if input_str in sign_dict:
        return sign_dict[input_str]
    else:
        raise FormulaError


def calc(input_str: str):
    eq = parse_eq(input_str)
    a = convert_to_float(eq[0])
    sign = eq[1]
    b = convert_to_float(eq[2])
    return sign_to_act(a, sign, b)


equation = input('Please enter equation in 1 + 1 format: ')
print(calc(equation))
