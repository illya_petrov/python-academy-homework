"""2) Определите класс итератор ReverseIter, который принимает список
и итерируется по нему в обратном направлении"""


class ReverseIter:
    def __init__(self, iter_list: list):
        self.iter_list = iter_list

    def __iter__(self):
        self.iter = -1
        return self

    def __next__(self):
        if self.iter >= len(self.iter_list) * -1:
            x = self.iter_list[self.iter]
            self.iter -= 1
            return x
        else:
            raise StopIteration


val = list(input('Some string - '))
print(f'Original list - {val}')
rev_val = ReverseIter(val)
iter_val = iter(rev_val)

print('Reversed list - ', end='')
for i in range(len(val)):
    print(next(iter_val), end=', ')

